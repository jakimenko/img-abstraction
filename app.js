const mongoose = require('mongoose');
const express = require('express');
const logger = require('morgan');

const index = require('./routes/index');
const apiRoutes = require('./routes/api');

const app = express();

mongoose.connect(process.env.MONGO_URI);

app.use(logger('dev'));

app.use('/api', apiRoutes);
app.use('/', index);

module.exports = app;

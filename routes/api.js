const express = require('express');
const request = require('request');
const querystring = require('querystring');

const Query = require('../models/query.js');

const router = express.Router();

router.get('/', (req, res, next) => {
  res.end('Image Search Abstraction Layer');
});

router.get('/latest/imagesearch', (req, res, next) => {
  Query.
    find({}).
    select({_id:0, queryString: 1, timestamp: 1}).
    sort({timestamp: -1}).
    exec((err, docs) => {
      if (err) console.log(err);
      res.json(docs);
    });
})

router.use('/imagesearch/:searchstring', (req, res, next) => {
  const query = new Query({
    queryString: req.params.searchstring
  });
  query.save();
  next();
});

router.get('/imagesearch/:searchstring', (req, res, next) => {
  const searchString = req.params.searchstring;
  const offset = req.query.offset;

  const base = "https://api.cognitive.microsoft.com/bing/v5.0/images/search?";
  const query = querystring.stringify({
    q: searchString,
    count: 10,
    offset: offset || 0
  });

  const requestString = base + query;

  const options = {
    url: requestString,
    headers: {
      "Ocp-Apim-Subscription-Key": process.env.API_SECRET
    }
  }

  request(options, (err, response, body) => {
    if (err) console.log(err);
    let arr = JSON.parse(body).value;

    arr = arr.map((i) => {
      const regexp = /[^](https?:\/\/.+)&p/;
      const url = i.contentUrl;
      const obj = {
        url: regexp.exec(decodeURIComponent(url))[1],
        thumbnail: i.thumbnailUrl,
        snippet: i.name,
        context: i.hostPageDisplayUrl
      }
      return obj;
    });
    res.json(arr);
  });
});

module.exports = router;

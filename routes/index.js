const express = require('express');
const router = express.Router();

/* GET home page. */
router.get('/', (req, res, next) => {
  res.end('Image Search Abstraction Layer');
});

module.exports = router;

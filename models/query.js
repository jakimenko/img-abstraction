const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const querySchema = new Schema({
  queryString: { type: String, required: true },
  timestamp: { type: Date, default: Date.now }
});

module.exports = mongoose.model('Query', querySchema);
